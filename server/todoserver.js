"use strict";
exports.__esModule = true;
exports.User = void 0;
var express = require("express");
var bodyParser = require("body-parser");
var User = /** @class */ (function () {
    function User(id, username, password, firstName, lastName, creationTime, rights) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.creationTime = creationTime;
        this.rights = rights;
    }
    return User;
}());
exports.User = User;
var userList;
userList = [
    new User(0, 'admin', 'admin', 'Peter', 'Mustermann', new Date(), 0),
    new User(1, 'biene', 'bienemaja', 'Sabine', 'Fraumuster', new Date(), 1),
    new User(2, 'test', 'test', 'Test', 'Test', new Date(), 2),
];
var router = express();
router.use(bodyParser.json());
router.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
router.post('/nutzer', function (req, res) {
    var id = userList.length;
    var username = req.body.title.username;
    var password = req.body.title.password;
    var firstName = req.body.title.firstName;
    var lastName = req.body.title.lastName;
    var rights = req.body.title.rights;
    var user = new User(id, username, password, firstName, lastName, new Date(), rights);
    var message = "";
    if (user) {
        userList.push(user);
        message = user.firstName + " erfolgreich hinzugef\u00FCgt";
        res.status(201);
    }
    else {
        res.status(400);
        message = 'Keinen title angegeben';
    }
    res.send({ 'message': message });
});
router.get('/userlist', function (req, res) {
    var message;
    message = userList.length + ' ToDos wurden gefunden';
    res.status(200);
    res.json({
        'message': message,
        'userList': userList
    });
});
router["delete"]('/entry/:id', function (req, res) {
    var id = Number(req.params.id);
    var message = '';
    if (!isNaN(id) && id >= 0 && id < userList.length && userList[id] != null) {
        message = userList[id].username + " wurde gel\u00F6scht.";
        userList.splice(id, 1);
        for (var i = 0; i < userList.length; i++) {
            userList[i].id = i;
        }
        res.status(200);
    }
    else if (isNaN(id)) {
        message = 'ID \'' + id + '\' ist keine Zahl';
        res.status(400);
    }
    else if (id < 0 || id >= userList.length) {
        message = 'ID ' + id + ' ist außerhalb des gültigen Bereichs';
        res.status(404);
    }
    else {
        message = 'Entry mit ID ' + id + ' wurde gelöscht';
        res.status(410);
    }
    res.json({ 'message': message });
});
router.put('/bearbeiten/:id', function (req, res) {
    var id = Number(req.params.id);
    var firstName = req.body.title.firstName;
    var lastName = req.body.title.lastName;
    var message = '';
    if (!isNaN(id) && id >= 0 && id < userList.length && userList[id] != null) {
        message = userList[id].username + " wurde erfolgreich bearbeitet.";
        userList[id].firstName = firstName;
        userList[id].lastName = lastName;
        res.status(200);
    }
    else if (isNaN(id)) {
        message = 'ID \'' + id + '\' ist keine Zahl';
        res.status(400);
    }
    else if (id < 0 || id >= userList.length) {
        message = 'ID ' + id + ' ist außerhalb des gültigen Bereichs';
        res.status(404);
    }
    else {
        message = 'Entry mit ID ' + id + ' wurde zuvor gelöscht';
        res.status(410);
    }
    res.json({ 'message': message });
});
router.use('/', express.static(__dirname + "/../todo/dist/todo"));
router.use('/*', express.static(__dirname + "/../todo/dist/todo"));
router.listen(8080, 'localhost', function () {
    console.log('');
    console.log('-------------------------------------------------------------');
    console.log('                    ToDo-Backend läuft                       ');
    console.log('-------------------------------------------------------------');
    console.log('       Liste abrufen:     http://localhost:8080/userlist     ');
    console.log('       Frontend aufrufen: http://localhost:8080              ');
    console.log('-------------------------------------------------------------');
});
//# sourceMappingURL=todoserver.js.map