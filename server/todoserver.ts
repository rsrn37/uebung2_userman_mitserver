import express    = require ('express');
import bodyParser = require ('body-parser');
import {Request, Response} from 'express';
import {Rights} from './rights';


export class User {
  public id: number;
  public username: string;
  public password: string;
  public firstName: string;
  public lastName: string;
  public creationTime: Date;
  public rights: Rights;

  constructor(id: number, username: string, password: string, firstName: string, lastName: string, creationTime: Date, rights: Rights) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.firstName = firstName;
    this.lastName = lastName;
    this.creationTime = creationTime;
    this.rights = rights;
  }
}


let userList: User[];

userList = [
  new User(0, 'admin', 'admin','Peter', 'Mustermann', new Date(), 0),
  new User(1, 'biene','bienemaja', 'Sabine', 'Fraumuster', new Date(), 1),
  new User(2, 'test','test', 'Test', 'Test', new Date(), 2),

];

let router = express();

router.use(bodyParser.json());

router.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

router.post('/nutzer', function (req: Request, res: Response) {
  let id: number = userList.length;
  let username: string = req.body.title.username;
  let password: string = req.body.title.password;
  let firstName: string = req.body.title.firstName;
  let lastName: string = req.body.title.lastName;
  let rights: number = req.body.title.rights;
  let user: User = new User(id,username,password,firstName,lastName,new Date(),rights)
  let message: string = "";

  if (user) {
    userList.push(user);
    message = `${user.firstName} erfolgreich hinzugefügt`;
    res.status(201);
  } else {
    res.status(400);
    message = 'Keinen title angegeben';
  }
  res.send({'message': message});
});

router.get('/userlist', function (req: Request, res: Response) {
  let message: string;
  message = userList.length + ' ToDos wurden gefunden';
  res.status(200);
  res.json({
    'message': message,
    'userList': userList
  });
});

router.delete('/entry/:id', function (req: Request, res: Response) {
  let id: number = Number(req.params.id);
  let message: string = '';
  if (!isNaN(id) && id >= 0 && id < userList.length && userList[id] != null) {
    message = `${userList[id].username} wurde gelöscht.`;
    userList.splice(id, 1);
    for (let i:number=0;i<userList.length;i++){
      userList[i].id =i;
    }
    res.status(200);
  } else if (isNaN(id)) {
    message = 'ID \'' + id + '\' ist keine Zahl';
    res.status(400);
  } else if (id < 0 || id >= userList.length) {
    message = 'ID ' + id + ' ist außerhalb des gültigen Bereichs';
    res.status(404);
  } else {
    message = 'Entry mit ID ' + id + ' wurde gelöscht';
    res.status(410);
  }
  res.json({'message': message});
});

router.put('/bearbeiten/:id', function(req: Request, res: Response) {
  let id: number = Number(req.params.id);
  let firstName: string = req.body.title.firstName;
  let lastName: string = req.body.title.lastName;
  let message: string = '';
  if (!isNaN(id) && id >= 0 && id < userList.length && userList[id] != null) {
    message = `${userList[id].username} wurde erfolgreich bearbeitet.`;
    userList[id].firstName = firstName;
    userList[id].lastName = lastName;
    res.status(200);
  } else if (isNaN(id)) {
    message = 'ID \'' + id + '\' ist keine Zahl';
    res.status(400);
  } else if (id < 0 || id >= userList.length) {
    message = 'ID ' + id + ' ist außerhalb des gültigen Bereichs';
    res.status(404);
  } else {
    message = 'Entry mit ID ' + id + ' wurde zuvor gelöscht';
    res.status(410);
  }
  res.json({'message': message});
});


router.use('/', express.static(`${__dirname}/../todo/dist/todo`));
router.use('/*', express.static(`${__dirname}/../todo/dist/todo`));

router.listen(8080, 'localhost', function () {
  console.log('');
  console.log('-------------------------------------------------------------');
  console.log('                    ToDo-Backend läuft                       ');
  console.log('-------------------------------------------------------------');
  console.log('       Liste abrufen:     http://localhost:8080/userlist     ');
  console.log('       Frontend aufrufen: http://localhost:8080              ');
  console.log('-------------------------------------------------------------');
});
