"use strict";
exports.__esModule = true;
exports.Rights = void 0;
// Enum for different access levels
var Rights;
(function (Rights) {
    Rights[Rights["User"] = 0] = "User";
    Rights[Rights["Admin"] = 1] = "Admin";
    Rights[Rights["SuperAdmin"] = 2] = "SuperAdmin";
})(Rights = exports.Rights || (exports.Rights = {}));
//# sourceMappingURL=Rights.js.map