import { Injectable } from '@angular/core';
import {User} from './User';
import {AddTodoComponent} from './add-todo/add-todo.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Rights} from './Rights';
import {HttpClient} from '@angular/common/http';
import {DeleteTodoComponent} from './delete-todo/delete-todo.component';
import {InsertuserTodoComponent} from './insertuser-todo/insertuser-todo.component';


@Injectable({
  providedIn: 'root'
})

export class DataService {
  public userList: User[];

  constructor(private modalService: NgbModal, private http: HttpClient) {
    this.http.get('http://localhost:8080/userlist').toPromise().then((res: any) => {
    this.userList = res.userList;
  }); }

  public async loeschen(index: number): Promise <void> {
    const modal = this.modalService.open(DeleteTodoComponent);
    try {
      const title = await modal.result;
      this.http.delete('http://localhost:8080/entry/' + index).toPromise().then((res: any) => {
      });
      this.http.get('http://localhost:8080/userlist').toPromise().then((res: any) => {
        this.userList = res.userList;
      });
    } catch (e) {
      console.log('Modal closed', e);
    }
  }

  public destroy(index: number): void {
    this.http.delete('http://localhost:8080/entry/' + index).toPromise().then((res: any) => {
    });
    this.http.get('http://localhost:8080/userlist').toPromise().then((res: any) => {
      this.userList = res.userList;
    });
  }

  async insertuser(): Promise <void> {
    const modal = this.modalService.open(InsertuserTodoComponent);
    const user = new User(0, '', '', '', new Date(), 0);
    modal.componentInstance.user = user;
    try {
      const title = await modal.result;
      this.http.post('http://localhost:8080/nutzer', {title}).toPromise().then((res: any) => {
        const message = res.message;
      });
      this.http.get('http://localhost:8080/userlist').toPromise().then((res: any) => {
        this.userList = res.userList;
      });
    }catch (e){
      console.log('Modal closed', e);
    }
  }

  async add(id: number, username: string, firstName: string, lastName: string, creationTime: Date, rights: Rights): Promise<void> {
    const modal = this.modalService.open(AddTodoComponent);
    const user = new User(id, username, firstName, lastName, creationTime, rights);
    modal.componentInstance.user = user;
    try {
      const title = await modal.result;
      this.http.put('http://localhost:8080/bearbeiten/' + id, {title}).toPromise().then((res: any) => {
        const message = res.message;
      });
      this.http.get('http://localhost:8080/userlist').toPromise().then((res: any) => {
        this.userList = res.userList;
      });
    }catch (e){
      console.log('Modal closed', e);
    }
  }
}
