import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {User} from '../User';

@Component({
  selector: 'app-insertuser-todo',
  templateUrl: './insertuser-todo.component.html',
  styleUrls: ['./insertuser-todo.component.css']
})
export class InsertuserTodoComponent implements OnInit {

  title = 'Userman';

  @Input() user: User;
  @Output() insertuser: EventEmitter<void> = new EventEmitter<void>();

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  insert(): void {
      this.activeModal.close(this.user);
  }
}
