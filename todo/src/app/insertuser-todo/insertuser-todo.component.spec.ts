import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InsertuserTodoComponent } from './insertuser-todo.component';

describe('InsertuserTodoComponent', () => {
  let component: InsertuserTodoComponent;
  let fixture: ComponentFixture<InsertuserTodoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InsertuserTodoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InsertuserTodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
