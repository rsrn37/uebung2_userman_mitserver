import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../User';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {

  @Input() user: User;
  @Output() add: EventEmitter<void> = new EventEmitter<void>();
  @Output() loeschen: EventEmitter<void> = new EventEmitter<void>();
  @Output() destroy: EventEmitter<void> = new EventEmitter<void>();
  constructor() { }

  ngOnInit(): void {
  }

}
