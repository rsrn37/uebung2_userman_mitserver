import {Component, EventEmitter, Output} from '@angular/core';
import {InsertuserTodoComponent} from "./insertuser-todo/insertuser-todo.component";
import {DataService} from "./data.service";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Userman';
  liste = 'Liste';
  hinzufuegen = 'Hinzufügen';

  @Output() insertuser: EventEmitter<void> = new EventEmitter<void>();


}
