import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {User} from '../User';
import {Rights} from '../Rights';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {

  title = 'Userman';

  @Input() user: User;
  @Output() add: EventEmitter<void> = new EventEmitter<void>();



  constructor(public activeModal: NgbActiveModal) {

  }

  ngOnInit(): void {
  }

  save(): void {
    if (this.user.firstName && this.user.lastName) {
      this.activeModal.close(this.user);
    }
  }
}
