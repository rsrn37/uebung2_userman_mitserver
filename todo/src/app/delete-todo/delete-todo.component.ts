import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {User} from '../User';

@Component({
  selector: 'app-delete-todo',
  templateUrl: './delete-todo.component.html',
  styleUrls: ['./delete-todo.component.css']
})
export class DeleteTodoComponent implements OnInit {
  public title = '';

  @Input() user: User;
  @Output() destroy: EventEmitter<void> = new EventEmitter<void>();

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit(): void {
  }
  d(): void {
    this.activeModal.close();
  }
}
